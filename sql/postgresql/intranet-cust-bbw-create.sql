-- cognovis GmbH

SELECT im_dynfield_attribute_new ('event_management_event', 'lottery_run_p','Lottery run?', 'checkbox', 'boolean', 't', 10, 'f', 'event_management_events')

SELECT im_dynfield_widget__new (
    null, 'im_dynfield_widget', now(), 0, '0.0.0.0', null,
    'dance_role_id', 'Dance Role', 'Dance Role',
    10007, 'integer', 'im_category_tree', 'integer',
    '{custom {category_type "Event Participant Type" translate_p 1}}'
);


SELECT im_category_new((select nextval('im_categories_seq'))::integer, 'Lead', 'Event Participant Type');
SELECT im_category_new((select nextval('im_categories_seq'))::integer, 'Follow', 'Event Participant Type');


alter table im_event_participants add column party_pass_interested_p boolean;
SELECT im_dynfield_attribute_new ('im_event_participant', 'party_pass_interested_p','Party Pass', 'checkbox', 'boolean', 't', 14, 'f', 'im_event_participants');


