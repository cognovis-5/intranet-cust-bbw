--upgrade-5.0.0.0.1-5.0.0.0.2
SELECT acs_log__debug('/packages/intranet-cust-bbw/sql/postgresql/upgrade/upgrade-5.0.0.0.1-5.0.0.0.2.sql','');

create or replace function im_event_dance_course__html (
    p_project_id        integer,
    p_participant_id    integer
) returns varchar as 
$$
declare

    course_id im_event_participants.course%TYPE;
    party_pass_interested im_event_participants.party_pass_interested_p%TYPE;
    v_result varchar;
    
begin

    v_result := '';

    SELECT course INTO course_id FROM im_event_participants WHERE participant_id = p_participant_id AND project_id = p_project_id;
    SELECT party_pass_interested_p INTO party_pass_interested FROM im_event_participants WHERE participant_id = p_participant_id AND project_id = p_project_id;

    if course_id = 12264 then
        v_result := v_result || 'Party Pass';
    else 
        if party_pass_interested = 't' then
            v_result := v_result || 'Workshop' || '<div style="color:green;">' || '(interested in party pass)' || '</div>';
        else 
            v_result := v_result || 'Workshop' || '<div style="color:red;">' || '(not interested in party pass)' || '</div>';
        end if;

    end if;

   return v_result; 


end;
$$ language 'plpgsql';

update im_view_columns set column_render_tcl = '$course_text_html', extra_select = 'im_event_dance_course__html(project_id,participant_id) as course_text_html' where column_id = 1003;
