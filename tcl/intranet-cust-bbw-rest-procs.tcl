ad_library {
    Rest Procedures for intranet-cust-bbw
    
    @author michaldrn@wp.pl
    @author malte.sussdorff@cognovis.de

}

namespace eval cog_rest::json_object {
    ad_proc bbw_registration {} {
        @return participant_id integer Participation_id for this event
        @return participant json_object user User who is participating
        @return first_names string first names of event participant
        @return last_name string last name of event participant
        @return email string email of event participant
        @return project named_id project is used as event id (single event = single project_id)
        @return ha_line1 string participant company address line 1
        @return ha_city string participant city name
        @return ha_state string participant company state
        @return ha_country_code string string participant company country
        @return ha_postal_code string particippant postal code (or zip)
        @return course_material_id integer Material of the course the user wants to attend
        @return event_participant_type category "Dance Role" Dance type - Lead or Follow
        @return event_participant_level category "Event Participant Level" Level of the participant
        @return event_partners_text string e.g. name of potential dance partners
        @return comments string Comments added by the participant
        @return party_pass_interested_p boolean is participant interested in party pass?
    } -

    ad_proc bbw_registration_body {} {
        @param project_id object im_project::* project_id is used as event id (single event = single project_id)
        @param first_names string first names of event participant
        @param last_name string last name of event participant
        @param email string email of event participant
        @param ha_line1 string participant company address line 1
        @param ha_city string participant city name
        @param ha_state string participant company state
        @param ha_country_code string string participant company country
        @param ha_postal_code string particippant postal code (or zip)
        @param course_material_id integer Material of the course the user wants to attend
        @param event_participant_type_id category "Dance Role" Dance type - Lead or Follow
        @param event_participant_level_id category "Event Participant Level" Level of the participant
        @param event_partners_text string e.g. name of potential dance partners
        @param party_pass_interested_p boolean is participant interested in party pass?
        @param comments string Comments added by the participant
    } - 


}

ad_proc -public cog_rest::get::bbw_registration {
    { -project_id ""}
    { -participant_id ""}
    -rest_user_id:required
} {
    Returns the registration for a BBW event

    @param project_id object im_project::* project_id is used as event id (single event = single project_id)
    @param email string email of event participant
    @param participant_id object im_person::read Participation inforamtion

    @return registration json_object bbw_registration The info from the actual registration

} {
    if {$participant_id eq ""} {
        if {$project_id eq ""} {
           set project_id [db_string project "select max(project_id) from im_projects where project_status_id = [im_project_status_open] and project_type_id = [im_project_type_event]" -default ""]
        }

        if {$project_id eq ""} {
            cog_rest::error -http_status 500 -message "No active event found for which we can register"
            return
        }

        set participant_id [db_string check_for_email "select participant_id from im_event_participants where person_id = :rest_user_id and project_id =:project_id " -default ""]
    }
    if {$participant_id eq ""} {
        cog_rest::error -http_status 403 -message "Could not find a registration you are allowed to view"
        return
    } 

    db_1row event_participant "select ep.participant_id, ep.project_id, ep.course as course_material_id, ep.person_id,
        ep.event_participant_type_id, ep.party_pass_interested_p, ep.event_partners_text, ep.event_participant_level_id,
        p.first_names, p.last_name, pa.email,
        uc.ha_line1,uc.ha_city,uc.ha_state,uc.ha_country_code,uc.ha_postal_code
        from im_event_participants ep 
        inner join parties pa on (pa.party_id=ep.person_id) 
        inner join persons p on (p.person_id=ep.person_id) 
        inner join users_contact uc on (uc.user_id=ep.person_id)
        where participant_id=:participant_id"

    set comments [db_string get_note "select note from im_notes where object_id = :participant_id and note_type_id = [im_note_type_participant]" -default ""]
    set participant [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::user -user_ids $person_id -rest_user_id $rest_user_id]]

    # Quick overwrite
    set first_names "you know"
    set last_name "you know"
    set ha_line1 "you should know"
    set ha_city "look around"
    set ha_city "Somewhere"
    set ha_country_code ""
    set ha_postal_code "123581321"
    set event_partners_text "MeMyselAndI@Somebody.el.se"
    set comments "Well... who knows"
    set participant ""
    set registration [cog_rest::json_object]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::bbw_registration {
    {-project_id ""}
    { -first_names ""}
    { -last_name ""}
    { -email ""}
    -ha_line1:required
    -ha_city:required
    { -ha_state ""}
    -ha_country_code:required
    -ha_postal_code:required
    -course_material_id:required
    -event_participant_type_id:required
    { -event_participant_level_id ""}
    { -event_partners_text ""}
    { -party_pass_interested_p 0}
    { -comments ""}
    -rest_user_id:required
} {
    Handler for new registrations 

    @param bbw_registration_body request_body Registration info for the bbw

    @return registration json_object bbw_registration The info from the actual registration
} {

    if {$email eq ""} {
        set email [party::email -party_id $rest_user_id]
    } 

    if {$first_names eq ""} {
        set first_names [db_string first "select first_names from persons where person_id = :rest_user_id" -default ""]
    }

    if {$last_name eq ""} {
        set last_name [db_string first "select last_name from persons where person_id = :rest_user_id" -default ""]
    }

    if {$project_id eq ""} {
        set project_id [db_string project "select max(project_id) from im_projects where project_status_id = [im_project_status_open] and project_type_id = [im_project_type_event]" -default ""]
    }

    if {$project_id eq ""} {
        cog_rest::error -http_status 500 -message "No active event found for which we can register"
        return
    }

    # Check if we have a registration for this user
    set participant_id [db_string check_for_email "select participant_id from im_event_participants ep, cc_users u where u.person_id = ep.person_id and u.email = :email and project_id =:project_id " -default ""]
    if {$participant_id eq ""} {
        # get a new acs object_id
        set participant_id [im_new_object_id]

        # Create the participant (no dynfields)
        set registration_successful_p [::event_management::create_participant \
            -participant_id $participant_id \
            -project_id $project_id \
            -email $email \
            -first_names $first_names \
            -last_name $last_name \
            -accepted_terms_p 1 \
            -course $course_material_id \
            -accommodation "" \
            -alternative_accommodation "" \
            -accommodation_comments "" \
            -food_choice "" \
            -roommates_text "" \
            -comments $comments \
            -event_partners_text $event_partners_text \
            -event_participant_type_id $event_participant_type_id \
            -cell_phone "" \
            -ha_line1 $ha_line1 \
            -ha_city $ha_city \
            -ha_postal_code $ha_postal_code \
            -ha_state $ha_state \
            -ha_country_code $ha_country_code \
            -no_callback]

        if {$registration_successful_p} {
            db_dml update_partipants "update im_event_participants set party_pass_interested_p = :party_pass_interested_p, event_participant_level_id = :event_participant_level_id where participant_id = :participant_id"
        }
    } else {
        db_dml update_partipants "update im_event_participants set 
            party_pass_interested_p = :party_pass_interested_p, event_participant_level_id = :event_participant_level_id 
            where participant_id = :participant_id"
    }
    cog::callback::invoke im_event_participant_after_create -object_id $participant_id

    set registration [cog_rest::json_object]
    return [cog_rest::json_response]
}


ad_proc -public im_rest_get_custom_sencha_berlinbalboa_registration {
    -project_id
    { -participant_id 0}
    { -first_names ""}
    { -last_name ""}
    { -email ""}
    { -cell_phone ""}
    { -ha_line1 ""}
    { -ha_city ""}
    { -ha_state ""}
    { -ha_country_code ""}
    { -ha_postal_code ""}
    { -course ""}
    { -event_participant_type_id ""}
    { -event_partners_text ""}
    { -party_pass_interested_p 0}
    { -website ""}
    { -arrival_time ""}
    { -accommodation ""}
    { -alternative_accommodation ""}
    { -accommodation_comments ""}
    { -roommate_preferences ""}
    { -roommates_text ""}
    { -food_choice ""}
    { -special_food_requests ""}
    { -adult_p 1}
    { -playapplication_url ""}
    { -twitter ""}
    { -professional_profile ""}
    { -p4a_arrival_time ""}
    { -comments ""}
    { -p_token ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Dummy endpoint for future registration for berlinbalboa
    
    @param project_id project_id is used as event id (single event = single project_id)
    @param participant_id id of participant. This decides if we are adding completely new participant or edit existing one
    @param first_names first names of event participant
    @param last_name last name of event participant
    @param email email of event participant
    @param cell_phone mobile phone number of participant
    @param ha_line1 participant company address line 1
    @param ha_city participant city name
    @param ha_state participant company state
    @param ha_country_code participant company country
    @param ha_postal_code particippant postal code (or zip)
    @param event_participant_type_id Dance type - Lead or Follow
    @param event_partners_text  e.g. name of potential dance partners
    @param party_pass_interested_p is participant interested in party pass?
    @param website website of participant
    @param arrival_time arrival time of participant
    @param accommodation Is particpant willing to share a room?
    @param alternative_accommodation alternative accommodation
    @param accommodation_comments additional comments related to accommodation
    @param roommate_preferences participant room preferences 
    @param roommates_text e.g. name of potential roommates
    @param food_choice food preferences of participant
    @param special_food_requests special food requests of participant
    @param playapplication_url url of playpplication
    @param twitter twitter id of participant
    @param professional_profile linkedin url of participant
    @param p4a_arrival_time arrival time of participant
    @param comments additional comments that user inputed during registration
    @param p_token token of user used when editing participant personal info

    @return participant_id newly created event participant_id

}  {

    ns_log Notice "Attempting to create new event participant for berlinbalboa"

    set obj_ctr 0
    set accepted_terms_p 1

    set dummy_results_json [list]

    array set doc_elements [nsv_get api_proc_doc im_rest_get_custom_sencha_berlinbalboa_registration]
    foreach param $doc_elements(param) {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }

        set params [split $param]
        set name [lindex $params 0]
        eval "set value $$name"
        append dummy_results_json "$komma{\"$name\":\"$value\"}"

        incr obj_ctr
    }

    # If participant_id is 0, we are creating completely new participant

    if {$participant_id eq 0} {

        # get a new acs object_id
        set participant_id [im_new_object_id]

        # Create the participant (no dynfields)
        set registration_successful_p [::event_management::create_participant \
            -participant_id $participant_id \
            -project_id $project_id \
            -email $email \
            -first_names $first_names \
            -last_name $last_name \
            -accepted_terms_p $accepted_terms_p \
            -course $course \
            -accommodation $accommodation \
            -alternative_accommodation $alternative_accommodation \
            -accommodation_comments $accommodation_comments \
            -food_choice $food_choice \
            -roommates_text $roommates_text \
            -event_partners_text $event_partners_text \
            -event_participant_type_id $event_participant_type_id \
            -cell_phone $cell_phone \
            -ha_line1 $ha_line1 \
            -ha_city $ha_city \
            -ha_postal_code $ha_postal_code \
            -ha_state $ha_state \
            -ha_country_code $ha_country_code \
            -comments $comments \
            -no_callback]

        if {$registration_successful_p} {
        # Dynfields
            set dynfields [im_dynfield::sorted_attributes -object_type im_event_participant]
            foreach dynfield $dynfields {
                eval "set value $$dynfield"

                if {$value ne ""} {
                    set attribute_id [db_string get_attribute_id "select da.attribute_id from im_dynfield_attributes da, acs_attributes aa, im_dynfield_layout la where da.acs_attribute_id = aa.attribute_id and da.attribute_id = la.attribute_id and aa.attribute_name = :dynfield limit 1" -default ""]
                    if {$attribute_id ne ""} {
                        db_dml save_event_participant_dynfield "insert into im_dynfield_attr_multi_value (attribute_id, object_id, value) values (:attribute_id, :participant_id, :value)"
                    }
                    # We need to execute additional sql in case column was also added to to im_event_participants_table
                    if {[im_column_exists "im_event_participants" $dynfield]} {
                        db_dml save_to_table "update im_event_participants set $dynfield =:value where participant_id=:participant_id"
                    }
                }
            }

            set result "{\"success\": true, \"participant_id\":$participant_id, \n\"inputed_data\": \[\n$dummy_results_json\n\]\n}"
            
            catch {
                callback im_event_participant_after_create -object_id $participant_id
            } err_msg

        } else {
            set result "{\"success\": false, \"message\":\"Such email is already registered in that event\"}"
        }


    } else {
        # If participant_id is not 0, we are editing existing participant. For that purpose we also need p_token to verify participant with user token
        # Get user id from participant_id
        set p_user_id [db_string get_user_id_from_participant_id "select person_id as p_user_id from im_event_participants where participant_id =:participant_id" -default 0]
        set token [im_generate_auto_login -user_id $p_user_id]
        if {$p_user_id ne 0 && $token eq $p_token} {
            
            im_user_update_existing_user \
            -user_id $p_user_id \
            -email $email \
            -username $email \
            -first_names $first_names\
            -last_name $last_name
            
            event_management::set_user_contact_info \
            -user_id $p_user_id \
            -ha_country_code $ha_country_code \
            -ha_line1 $ha_line1 \
            -ha_city $ha_city \
            -ha_state $ha_state \
            -ha_postal_code $ha_postal_code

            # Save dynfields
            set dynfields [im_dynfield::sorted_attributes -object_type im_event_participant]

            # Little hack in here for now we manually add some fields to dynfields table. We need to start storing these values as dynfields
            lappend dynfields "event_participant_type_id"
            lappend dynfields "event_partners_text"
            lappend dynfields "food_choice"
            lappend dynfields "course"

            foreach dynfield $dynfields {
                eval "set value $$dynfield"
                if {$value ne ""} {
                    set attribute_id [db_string get_attribute_id "select da.attribute_id from im_dynfield_attributes da, acs_attributes aa, im_dynfield_layout la where da.acs_attribute_id = aa.attribute_id and da.attribute_id = la.attribute_id and aa.attribute_name = :dynfield limit 1" -default ""]
                    if {$attribute_id ne ""} {
                        db_dml update_event_participant_dynfield "update im_dynfield_attr_multi_value set value=:value where attribute_id =:attribute_id and object_id =:participant_id"
                    }
                    if {[im_column_exists "im_event_participants" $dynfield]} {
                        db_dml save_to_table "update im_event_participants set $dynfield =:value where participant_id=:participant_id"
                    }
                }
            }

            # Take care of event partners
            set event_partners_list [lsearch -all -inline -not [split $event_partners_text ",|\t\n\r"] {}]

            foreach event_partner_text $event_partners_list {
                # remove old partner record
                db_dml remove_old_partner "delete from im_event_partners where project_id =:project_id and participant_id =:participant_id"
                ::event_management::match_name_email $event_partner_text event_partner_name event_partner_email
                db_exec_plsql insert_event_partner "select im_event_partner__new(
                    :participant_id,
                    :project_id,
                    :event_partner_email,
                    :event_partner_name
                )"
            }

        }

        set result "{\"success\": true, \"participant_id\":$participant_id, \n\"inputed_data\": \[\n$dummy_results_json\n\]\n}"
        
    }


    
    im_rest_doc_return 200 "application/json" $result
    return

}




ad_proc -public im_rest_get_custom_sencha_berlinbalboa_waiting_spot_request {
    -email
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Provides waiting spot information link
} {
    
    set success false
    set message "User with email $email doest not exist"
    set participant_id ""
    set ws_token ""
    set waiting_spot 0
    set link ""

    set locale ""


    db_0or1row get_participant_info "select ep.participant_id, ep.project_id, ep.event_participant_status_id, u.first_names, u.last_name, u.user_id as ws_user_id from im_event_participants ep, cc_users u where u.user_id = ep.person_id and u.email =:email limit 1"
    if {$participant_id ne ""} {

        set locale [lang::user::locale -user_id $ws_user_id]
        set ws_token [im_generate_auto_login -user_id $ws_user_id]
        set sencha_registration_app_url [parameter::get_from_package_key -package_key "intranet-event-management" -parameter "SenchaRegistrationAppUrl" -default ""]
        set link "$sencha_registration_app_url/?page=waiting-spot&ws_token=$ws_token&ws_user_id=$ws_user_id"

        set event_participant_status_name [im_name_from_id $event_participant_status_id]
        
        # We get event_name which is project_name from im_projects 
        set event_name [im_name_from_id $project_id]
        
        set subject [lang::message::lookup $locale intranet-cust-bbw.status_info_request_email_subject "$event_name - your participation status"]
        set default_body [lang::message::lookup $locale intranet-cust-bbw.status_info_request_email_default_body ""]

        # Check if lottery was run
        set waiting_spot_info ""
        if {[im_dynfield::attribute::exists_p -object_type "event_management_events" -attribute_name "lottery_run_p"]} {
            set lottery_run_p [db_string check_lottery "select 1 from event_management_events where project_id=:project_id and lottery_run_p = '1'" -default 0]
            if {$lottery_run_p eq 1} {
                set waiting_spot [event_management_participant_waiting_spot -participant_id $participant_id]
                set waiting_spot_info [lang::message::lookup $locale intranet-cust-bbw.waiting_spot_info ""]
            } 
        }

        # Now that we have participant_id, we first want to check user status and send different mail
        switch $event_participant_status_id {

            82500 {
                # Participant is currently on waiting list
                # We can return his waiting_spot
                set waiting_spot [event_management_participant_waiting_spot -participant_id $participant_id]
                set body [lang::message::lookup $locale intranet-cust-bbw.waiting_spot_info_request_email_body "Your waiting spot:$waiting_spot"]
            }

            default {
                set body $default_body
            }

        }

        set success true
        set admin_email [ad_host_administrator]
        
        set message "Please check your email"

        # Send emails
        acs_mail_lite::send \
            -send_immediately \
            -to_addr $email \
            -from_addr $admin_email \
            -subject $subject \
            -body $body \
            -mime_type "text/html" \
            -object_id $participant_id \
            -no_callback \
            -use_sender

    }

    set result "{\"success\": $success,\"message\":\"$message\", \"link\":\"$link\"}"
    im_rest_doc_return 200 "application/json" $result
    return

}


ad_proc -public im_rest_get_custom_sencha_berlinbalboa_waiting_spot_display {
    -ws_token
    -ws_user_id
    { -ws_token ""}   
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Provides waiting spot information for berlinbalboa participant
} {

    set success false
    set message "Wrong token !"
    set result "{\"success\": $success,\"message\":\"$message\"}"
    
    set participant_veryify_p [db_string get_participant "select 1 from im_event_participants ep, cc_users u where u.user_id = ep.person_id and ep.person_id =:ws_user_id limit 1 " -default 0]
    
    if {$participant_veryify_p} {

        db_0or1row get_participant_info "select u.email, ep.participant_id, ep.project_id, ep.event_participant_status_id, u.first_names, u.last_name from im_event_participants ep, cc_users u where u.user_id = ep.person_id and ep.person_id =:ws_user_id limit 1"
        set real_token [im_generate_auto_login -user_id $ws_user_id]
        if {$participant_id ne "" && ($ws_token eq $real_token)} {
            if {$ws_token eq $real_token} {
                set waiting_spot [event_management_participant_waiting_spot -participant_id $participant_id]
                set success true
                set message "Here is your data!"
                set result "{\"success\": $success,\"message\":\"$message\", \"participant_id\":$participant_id,\"project_id\":$project_id, \"event_participant_status_id\":\"$event_participant_status_id\", \"event_participant_status_name\":\"[im_name_from_id $event_participant_status_id]\",\"email\":\"$email\",\"first_names\":\"$first_names\",\"last_name\":\"$last_name\", \"waiting_spot\":\"$waiting_spot\"}"
            }
        }

    } 
    
    im_rest_doc_return 200 "application/json" $result
    return

}


ad_proc -public im_rest_get_custom_sencha_berlinbalboa_participation_status_update {
    -participant_id
    -status_id
    -project_id
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Provides waiting spot information for berlinbalboa participant
} {

    set success true
    set message "success"

    # We need "old_status_id",
    set old_status_id [db_string get_old_status_id "select event_participant_status_id from im_event_participants where participant_id =:participant_id" -default ""]

    set res [event_management_update_participation_status -participant_id $participant_id -status_id $status_id]
    
    # If the new status is "canceled" and participant was already registered then we must execute callback which will change staus of another participant
    # We send id of participant for whom we just changed status
    if {$status_id eq [event_management::status::cancelled]} {
        catch {
            callback im_event_participant_after_confirm -participant_id $participant_id
        } err_msg
    }

    set result "{\"success\": true}"
    im_rest_doc_return 200 "application/json" $result
    return

}


ad_proc -public im_rest_get_custom_sencha_participant_info {
    -project_id
    -p_user_id
    -p_token
    { -dynfields ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Endpoint for future registration for berlinbalboa
    
    @param project_id project_id is used as event id (single event = single project_id)
    @param p_user_id user_id of participant
    @param p_token token id of participant
    @param dynfieds Comma seperated dynfields list

    @return participant_id id of participant

}  {

    ns_log Notice "Getting partipant info"

    set success false
    set message "Wrong token !"
    set result "{\"success\": $success,\"message\":\"$message\"}"

    set dynfiedls_sql ""
    if {$dynfields ne ""} {
        set dynfiedls_sql ", $dynfields"
    }
    
    set participant_veryify_p [db_string get_participant "select 1 from im_event_participants ep, cc_users u where u.user_id = ep.person_id and ep.person_id =:p_user_id limit 1 " -default 0]

    if {$participant_veryify_p} {
        db_0or1row get_participant_info "select u.email, ep.participant_id, u.user_id, ep.project_id, ep.event_participant_status_id, u.first_names, u.last_name, uc.ha_country_code, uc.ha_line1, uc.ha_city, uc.ha_state, uc.ha_postal_code $dynfiedls_sql from im_event_participants ep, cc_users u, users_contact uc where u.user_id = ep.person_id and uc.user_id = ep.person_id and ep.person_id =:p_user_id limit 1"
        set real_token [im_generate_auto_login -user_id $p_user_id]

        if {$participant_id ne "" && ($p_token eq $real_token)} {
            set success true
            set message "Ok"
            set result "{"
            append result "\"success\": $success,\"message\":\"$message\", \"user_id\":$user_id,\"participant_id\":$participant_id,\"project_id\":$project_id, \"ha_country_code\":\"$ha_country_code\", \"ha_line1\":\"$ha_line1\", \"ha_city\":\"$ha_city\", \"ha_state\":\"$ha_state\", \"ha_postal_code\":\"$ha_postal_code\", \"event_participant_status_id\":\"$event_participant_status_id\", \"event_participant_status_name\":\"[im_name_from_id $event_participant_status_id]\",\"email\":\"$email\",\"first_names\":\"$first_names\",\"last_name\":\"$last_name\""
            foreach dynfield [split $dynfields ","] {
                eval "set value $$dynfield"
                append result ",\"$dynfield\":\"$value\""
            }
            append result "}"
        }
    }

    im_rest_doc_return 200 "application/json" $result
    return

}



ad_proc -public im_rest_get_custom_bbw_validate_partner {
    -project_id 
    -partner_email
    -my_role_id
    { -my_email ""}
    { -ws_user_id ""}
    { -ws_token ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Rest endpoint which allows to validate dance partner field

    @param project_id project id of event
    @param partner_email email of partner
    @param my_email email of user who is registering himself
    @param my_role role submitted by user (role of person who is trying to register himself)
    @param ws_user_id user_id used for verification
    @param ws_token token of participant

} {
    # Get rest user id locale
    set locale [lang::user::locale -user_id $rest_user_id]

    set success true
    set message_type ""
    set message "ok"

    # First we check if partner_email is empty. Empty value is CORRECT value and we should progress with such input
    if {$partner_email ne ""} {

        # Next we check if partner_email is valid email address
        set success false
        set message_type "error"
        set message "Error: This field should be an email address in the format 'user@example.com'"

        set is_email_valid_p [acs_mail_lite::utils::valid_email_p $partner_email]

        if {$is_email_valid_p} {

            # For now I disable check for existance in system as Extjs does not provide easy way to display warning
            set participant_registered_p [event_management_participant_email_already_registered -project_id $project_id -email $partner_email]    
            if {false} {
                set success false
                set message_type "warning"
                set message [lang::message::lookup $locale intranet-cust-bbw.dance_partner_does_not_exists_in_system "Warning: Does not exists in the system."]
            } else {
                set success false
                set message_type "error"
                set message [lang::message::lookup $locale intranet-cust-bbw.dance_partner_found_someone_else "Error: Dance partner has found somebody else. Please let them know your E-Mail if you still want to partner with them."]
                
                set selected_partner_of_partner [db_string get_partner_of_partner "select event_partner_email from im_event_partners partners, im_event_participants participants, cc_users users where users.user_id = participants.person_id and partners.participant_id = participants.participant_id and users.email =:partner_email" -default ""]
                
                # We also need to check if partner didn't input someone not registred. Otherwise app would throw info he already selected someone else
                set partner_of_partner_registered_p [db_string is_partner_of_partner_registered "select 1 from cc_users where email =:selected_partner_of_partner" -default 0] 
                
                if {$my_email eq $selected_partner_of_partner || $selected_partner_of_partner eq $partner_email || $selected_partner_of_partner eq "" || $partner_of_partner_registered_p eq 0} {
                    set message [lang::message::lookup $locale intranet-cust-bbw.dance_partner_wrong_participant_type "Error: Your dance partner does not have the opposite role."]
                    set partner_dance_role [db_string get_partner_dance_role "select event_participant_type_id from im_event_participants ep, cc_users u where ep.person_id = u.user_id and u.email =:partner_email" -default ""]
                    if {$partner_dance_role ne $my_role_id} {
                        set success true
                        set message_type ""
                        set message "ok"
                    }
                }
            }

        }
    }


    set result "{\"success\": $success, \"message\":\"$message\",\"message_type\":\"$message_type\"}"
    im_rest_doc_return 200 "application/json" $result
    return

} 

namespace eval cog_rest::post {
    ad_proc -public register_user {
         -email:required
         {-first_names ""}
         {-last_name ""}
    } {
        Registers a user for login. If names are provided, create a person in addition
        Allows callbacks to act upon it.

        @param email string E-Mail we want to register
        @param first_names string First names of the registered user
        @param last_name string Last Name of the registered user

        @return party json_object user_token Party with authorization information

    } {

        set user_id [party::get_by_email -email $email]
        if {$user_id eq ""} {
            array set user_info [auth::create_user -email $email -first_names $first_names -last_name $last_name]
            set user_id $user_info(user_id)
	}


        if {$user_id eq ""} {
            cog_rest::error -http_status 503 -message "[_ cognovis-rest.err_party_create]"
        } else {
	    im_profile::add_member -profile_id 461 -user_id $user_id
	    set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_category'" -default 0]]
            permission::grant -party_id $user_id -object_id $rest_otype_id -privilege "read"
            set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_material'" -default 0]]
            permission::grant -party_id $user_id -object_id $rest_otype_id -privilege "read"

            set token [im_generate_auto_login -user_id $user_id]
            set bearer_token [base64::encode "$user_id:$token"]
            set party [cog_rest::json_object]
        }
        return [cog_rest::json_response]
    }
}
